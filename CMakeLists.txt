cmake_minimum_required (VERSION 2.8.11)
project (egl_tearing_test)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
set (CMAKE_CXX_STANDARD 11)

include_directories(${PROJECT_BINARY_DIR}/src)

find_library( OPENGLES3_LIBRARY GLESv2 "OpenGL ES v3.0 library")
find_library( EGL_LIBRARY EGL "EGL 1.4 library" )
find_package(X11)

add_subdirectory(src)


