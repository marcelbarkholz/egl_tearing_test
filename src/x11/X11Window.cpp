/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * File:   Window.cpp
 * Author: marcel.barkholz
 * 
 * Created on 24. Februar 2018, 11:10
 */

#include "x11/X11Window.h"
#include "x11/X11Display.h"
#include "common/Assertion.h"

#include <iostream>
#include <cstring>
#include <cassert>
#include <X11/Xutil.h>

using namespace egltt;


//-----------------------------------------------------------------------------------------------------------------
X11Window::X11Window(const std::shared_ptr<X11Display>& _display, const std::string& _title, unsigned int _width, unsigned int _height, eMode _mode)
    :   display(_display),
        title(_title),
        width(_width),
        height(_height),
        mode(_mode),
        window(0)
{
    this->init();
}

//-----------------------------------------------------------------------------------------------------------------
X11Window::~X11Window()
{
    this->release();
}

//-----------------------------------------------------------------------------------------------------------------
void X11Window::init()
{
    Display* nativeDisplay = this->display->getNativeDisplay();
    ASSERT(nativeDisplay);
    
    Window root = DefaultRootWindow(nativeDisplay);

    if ( MODE_OVERRIDE_REDIRECT == this->mode ) {
        XWindowAttributes ra;
        XGetWindowAttributes(nativeDisplay, root, &ra);
        this->width  = ra.width;
        this->height = ra.height;
    }

    XSetWindowAttributes swa;
    swa.event_mask =  ExposureMask | PointerMotionMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask;
    this->window = XCreateWindow(
            nativeDisplay,
            root,
            0, 0,
            this->width,
            this->height,
            0,
            CopyFromParent, InputOutput,
            CopyFromParent, CWEventMask,
            &swa
            );
    this->wmDeleteMessage = XInternAtom(nativeDisplay, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(nativeDisplay, this->window, &this->wmDeleteMessage, 1);

    XSetWindowAttributes  xattr;
    xattr.override_redirect = MODE_OVERRIDE_REDIRECT == this->mode;
    XChangeWindowAttributes (nativeDisplay, this->window, CWOverrideRedirect, &xattr);

    XWMHints hints;
    hints.input = 1;
    hints.flags = InputHint;
    XSetWMHints(nativeDisplay, this->window, &hints);

    // make the window visible on the screen
    XMapWindow (nativeDisplay, this->window);
    XStoreName (nativeDisplay, this->window, this->title.c_str());

    if ( MODE_OVERRIDE_REDIRECT == this->mode ) {
        XGrabPointer(nativeDisplay, this->window, true, 0, GrabModeAsync, GrabModeAsync, this->window, None, CurrentTime);
        XGrabKeyboard(nativeDisplay, this->window, true, GrabModeAsync, GrabModeAsync, CurrentTime);
    }

    // get identifiers for the provided atom name strings
    Atom wm_state = XInternAtom (nativeDisplay, "_NET_WM_STATE", 0);
    Atom wm_fullscreen = XInternAtom(nativeDisplay, "_NET_WM_STATE_FULLSCREEN", true);

    XEvent xev;
    std::memset(&xev, 0, sizeof(xev));
    xev.type                 = ClientMessage;
    xev.xclient.window       = this->window;
    xev.xclient.message_type = wm_state;
    xev.xclient.format       = 32;
    xev.xclient.data.l[0]    = 1;

    if ( MODE_FULLSCREEN == this->mode ) {
        xev.xclient.data.l[1] = wm_fullscreen;
        xev.xclient.data.l[2] = 0;
    } else {
        xev.xclient.data.l[1] = 0;
    }

    XSendEvent(
            nativeDisplay,
            DefaultRootWindow ( nativeDisplay ),
            0,
            SubstructureRedirectMask | SubstructureNotifyMask,
            &xev
            );

    XMapRaised(nativeDisplay, this->window);
    XSync(nativeDisplay, True);
}

//-----------------------------------------------------------------------------------------------------------------
void X11Window::release()
{
    Display* nativeDisplay = this->display->getNativeDisplay();
    ASSERT(nativeDisplay);

    if ( this->window ) {
        XUnmapWindow(nativeDisplay, this->window);
        XDestroyWindow(nativeDisplay, this->window);
        this->window = 0;
    }
}

//-----------------------------------------------------------------------------------------------------------------
bool X11Window::handleMessages()
{
    Display* nativeDisplay = this->display->getNativeDisplay();
    ASSERT(nativeDisplay);
    
    XEvent xev;
    KeySym key;
    bool userinterrupt = false;
    char text = 0;

    while ( XPending ( nativeDisplay ) ) {
        XNextEvent( nativeDisplay, &xev );
        if ( KeyPress == xev.type ) {
            text = 0;
            if ( XLookupString(&xev.xkey, &text, 1, &key, 0) ) {
                if ( 32 > text || 126 < text ) {
                    text = 0;
                }
            }
            this->keyDownSignal(xev.xkey.keycode, xev.xkey.state, text);
        }
        else if ( KeyRelease == xev.type ) {
            text = 0;
            if ( XLookupString(&xev.xkey, &text, 1, &key, 0) ) {
                if ( 32 > text || 126 < text ) {
                    text = 0;
                }
            }
            this->keyUpSignal(xev.xkey.keycode, xev.xkey.state, text);
        }
        else if ( ButtonPress == xev.type ) {
            this->mouseDownSignal(xev.xbutton.button, xev.xbutton.state);
        }
        else if ( ButtonRelease == xev.type ) {
            this->mouseUpSignal(xev.xbutton.button, xev.xbutton.state);
        }
        else if ( ClientMessage == xev.type ) {
            if (xev.xclient.data.l[0] == wmDeleteMessage) {
                userinterrupt = true;
            }
        }
        else if ( DestroyNotify == xev.type ) {
            userinterrupt = true;
        }
    }
    return !userinterrupt;
}

//-----------------------------------------------------------------------------------------------------------------
const X11Window::KeyDownSignal_t& X11Window::getKeyDownSignal() const
{
    return this->keyDownSignal;
}

//-----------------------------------------------------------------------------------------------------------------
const X11Window::KeyUpSignal_t& X11Window::getKeyUpSignal() const
{
    return this->keyUpSignal;
}

//-----------------------------------------------------------------------------------------------------------------
const X11Window::MouseDownSignal_t& X11Window::getMouseDownSignal() const
{
    return this->mouseDownSignal;
}

//-----------------------------------------------------------------------------------------------------------------
const X11Window::MouseDownSignal_t& X11Window::getMouseUpSignal() const
{
    return this->mouseUpSignal;
}

//-----------------------------------------------------------------------------------------------------------------
void X11Window::close()
{
    Display* nativeDisplay = this->display->getNativeDisplay();
    ASSERT(nativeDisplay);
    
    XEvent ev;
    memset(&ev, 0, sizeof (ev));

    ev.xclient.type = ClientMessage;
    ev.xclient.window = window;
    ev.xclient.message_type = XInternAtom(nativeDisplay, "WM_PROTOCOLS", true);
    ev.xclient.format = 32;
    ev.xclient.data.l[0] = XInternAtom(nativeDisplay, "WM_DELETE_WINDOW", false);
    ev.xclient.data.l[1] = CurrentTime;
    XSendEvent(nativeDisplay, this->window, False, NoEventMask, &ev);
}

//-----------------------------------------------------------------------------------------------------------------
Window X11Window::getNativeWindow() const
{
    ASSERT(this->window);
    return this->window;
}

//-----------------------------------------------------------------------------------------------------------------
void X11Window::getDimension(unsigned int *width, unsigned int  *height) const
{
    Display* nativeDisplay = this->display->getNativeDisplay();
    ASSERT(nativeDisplay);
    ASSERT(this->window);

    Window root;
    int x, y;
    unsigned int border;
    unsigned int depth;

    XGetGeometry(nativeDisplay, this->window, &root, &x, &y, width, height, &border, &depth);
}

//-----------------------------------------------------------------------------------------------------------------
void X11Window::getMousePosition(int *x, int  *y) const
{
    Display* nativeDisplay = this->display->getNativeDisplay();
    ASSERT(nativeDisplay);
    ASSERT(this->window);

    Bool result;
    Window child;
    Window root;
    int root_x, root_y;
    unsigned int mask;

    result = XQueryPointer(nativeDisplay, this->window, &root, &child, &root_x, &root_y, x, y, &mask);

    if ( !result ) {
        *x = -1;
        *y = -1;
    }
}

//-----------------------------------------------------------------------------------------------------------------
X11Window::eMode X11Window::getMode() const
{
    return this->mode;
}