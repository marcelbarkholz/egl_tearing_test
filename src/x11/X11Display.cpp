/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * File:   X11Display.cpp
 * Author: marcel.barkholz
 * 
 * Created on 25. Februar 2018, 15:48
 */

#include "x11/X11Display.h"
#include "common/Assertion.h"

#include <iostream>

using namespace egltt;

//-----------------------------------------------------------------------------------------------------------------
int X11Display::XErrorHandler(Display* dpy, XErrorEvent* error)
{
    char buf[1024];
    XGetErrorText(error->display, error->error_code, buf, sizeof(buf));
    std::cerr << "ErrorHandler: " << buf << "\n"
              << "type:         " << error->type << "\n"
              << "serial:       " << error->serial << "\n"
              << "error_code:   " << (int)error->error_code << "\n"
              << "request_code: " << (int)error->request_code << "\n"
              << "minor_code:   " << (int)error->minor_code
              << std::endl;

    return 0;
}


//-----------------------------------------------------------------------------------------------------------------
X11Display::X11Display()
    :   display(nullptr)
{
    this->init();
    XSetErrorHandler(XErrorHandler);
}


//-----------------------------------------------------------------------------------------------------------------
X11Display::~X11Display()
{
    this->release();
}

//-----------------------------------------------------------------------------------------------------------------
void X11Display::init()
{
    this->display = XOpenDisplay(NULL);
    if ( !this->display ) {
        throw std::runtime_error("open x11 display failed");
    }
}

//-----------------------------------------------------------------------------------------------------------------
void X11Display::release()
{
    if ( this->display ){
        XCloseDisplay(this->display);
        this->display = NULL;
    }
}

//-----------------------------------------------------------------------------------------------------------------
Display* X11Display::getNativeDisplay() const
{
    ASSERT(this->display);
    return this->display;
}