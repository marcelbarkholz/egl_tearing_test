/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   Window.h
 * Author: marcel.barkholz
 *
 * Created on 24. Februar 2018, 11:10
 */

#ifndef WINDOW_H
#define WINDOW_H

#include <string>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include "common/Signal.h"

namespace egltt {

    class X11Display;

    //-----------------------------------------------------------------------------------------------------------------
    ///! \class X11Window
    class X11Window
    {
    public:
        typedef Signal<unsigned int, unsigned int, char> KeyDownSignal_t;
        typedef Signal<unsigned int, unsigned int, char> KeyUpSignal_t;

        typedef Signal<unsigned int, unsigned int> MouseDownSignal_t;
        typedef Signal<unsigned int, unsigned int> MouseUpSignal_t;

        enum eMode {
            MODE_WINDOWED,
            MODE_FULLSCREEN,
            MODE_OVERRIDE_REDIRECT,
        };

        X11Window(const std::shared_ptr<X11Display>& _display, const std::string& _title, unsigned int _width, unsigned int _height, eMode _mode);
        virtual ~X11Window();

        virtual bool handleMessages();

        const KeyDownSignal_t& getKeyDownSignal() const;
        const KeyUpSignal_t& getKeyUpSignal() const;

        const MouseDownSignal_t& getMouseDownSignal() const;
        const MouseDownSignal_t& getMouseUpSignal() const;

        void close();

        Window getNativeWindow() const;

        void getDimension(unsigned int *width, unsigned int  *height) const;
        void getMousePosition(int *x, int  *y) const;

        eMode getMode() const;

    private:
        void init();
        void release();

    private:
        std::shared_ptr<X11Display> display;
        std::string title;
        unsigned int width;
        unsigned int height;
        eMode mode;

        Window window;
        Atom wmDeleteMessage;

        KeyDownSignal_t keyDownSignal;
        KeyUpSignal_t keyUpSignal;

        MouseDownSignal_t mouseDownSignal;
        MouseUpSignal_t mouseUpSignal;
    };

} // namespace egltt

#endif /* WINDOW_H */

