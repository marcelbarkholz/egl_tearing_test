/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * File:   bar.cpp
 * Author: marcel.barkholz
 * 
 * Created on 25. Februar 2018, 12:57
 */

#include "Bar.h"
#include "gui/contrib/imgui_internal.h"
#include "Statistics.h"

using namespace egltt;

//-----------------------------------------------------------------------------------------------------------------
Bar::Bar()
    :   size(10.0f),
        speed(5.0f),
        position(0.0f),
        color(0.8f, 0.8f, 0.8f, 1.0f),
        visible(true)
{
}


//-----------------------------------------------------------------------------------------------------------------
Bar::~Bar()
{
}

//-----------------------------------------------------------------------------------------------------------------
void Bar::setSpeed(float _speed)
{
    this->speed = _speed;
}

//-----------------------------------------------------------------------------------------------------------------
float Bar::getSpeed() const
{
    return this->speed;
}

//-----------------------------------------------------------------------------------------------------------------
void Bar::setSize(float _size)
{
    this->size = _size;
}

//-----------------------------------------------------------------------------------------------------------------
float Bar::getSize() const
{
    return this->size;
}

//-----------------------------------------------------------------------------------------------------------------
void Bar::render(Statistics& statistics)
{    
    // is ImGui to draw the bar => we do not need to handle all the shader stuff our self...

    const ImVec2& displaySize = ImGui::GetIO().DisplaySize;
    
    // "fullscreen" window in the background
    ImGui::SetNextWindowPos(ImVec2(0,0));
    ImGui::SetNextWindowSize(displaySize);
    ImGuiWindowFlags flags
            = ImGuiWindowFlags_NoTitleBar
            | ImGuiWindowFlags_NoResize
            | ImGuiWindowFlags_NoMove
            | ImGuiWindowFlags_NoScrollbar
            | ImGuiWindowFlags_NoScrollWithMouse;
    ImGui::PushStyleColor(ImGuiCol_WindowBg, (ImU32)ImColor(0, 0, 0, 0));
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
    if ( !ImGui::Begin("bar window", &this->visible, ImVec2(0,0), 0.0f, flags) ) {
        ImGui::End();
        return;
    }

    if ( this->visible ) {
        auto measurementScope = statistics.measurement("bar rendering");

        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        draw_list->PushClipRectFullScreen();

        ImRect rect(this->position, 0.0f, this->position+this->size, displaySize.y);
        draw_list->AddRectFilled(rect.Min, rect.Max, this->color);

        if ( 0.0f < this->speed && rect.Max.x > displaySize.x) {
            float position2 = -(displaySize.x - rect.Min.x);
            rect = ImRect(position2, 0.0f, position2+this->size, displaySize.y);
            draw_list->AddRectFilled(rect.Min, rect.Max, this->color);

        } else if ( 0.0f > this->speed && rect.Min.x < 0.0f) {
            float position2 = displaySize.x + rect.Min.x;
            rect = ImRect(position2, 0.0f, position2+this->size, displaySize.y);
            draw_list->AddRectFilled(rect.Min, rect.Max, this->color);
        }

        draw_list->PopClipRect();
    }
    
    ImGui::End();
    ImGui::PopStyleVar();
    ImGui::PopStyleColor();

    this->position += this->speed;

    if ( 0.0f > this->speed ) {
        if ( 0.0f > this->position+this->size ) {
            this->position = displaySize.x - this->size;
        }
    } else if ( 0.0f < this->speed ) {
        if ( displaySize.x < this->position ) {
            this->position = 0.0f;
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Bar::options()
{
    ImGui::PushID(this);
    ImGui::Checkbox("visible", &this->visible);
    ImGui::DragFloat("size", &this->size, 0.1f);
    ImGui::DragFloat("speed", &this->speed, 0.1f, -20.0f, 20.0f);
    ImGui::ColorEdit4("color", &this->color.Value.x);
    ImGui::PopID();
}

//-----------------------------------------------------------------------------------------------------------------
void Bar::setVisible(bool visible)
{
    this->visible = visible;
}

//-----------------------------------------------------------------------------------------------------------------
bool Bar::isVisible() const
{
    return visible;
}