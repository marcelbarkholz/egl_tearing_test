/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "Application.h"
#include "x11/X11Display.h"
#include "x11/X11Window.h"
#include "egl/Context.h"
#include "gui/ImmediateGui.h"
#include "common/Assertion.h"
#include "Bar.h"
#include "Statistics.h"

using namespace egltt;

//-----------------------------------------------------------------------------------------------------------------
Application::Application(const std::string& _name)
    :   name(_name),
        windowMode(X11Window::MODE_WINDOWED),
        clearColor(0.0f, 0.0f, 0.0f, 1.0f),
        glFinishBeforeAfterSwap(0),
        display(nullptr),
        window(nullptr),
        context(nullptr),
        gui(nullptr),
        bar(nullptr),
        statistics(nullptr)
{
    // ...
}

//-----------------------------------------------------------------------------------------------------------------
Application::~Application()
{
    // ...
}

//-----------------------------------------------------------------------------------------------------------------
bool Application::arguments(int /*argc*/, char** /*argv*/)
{
    return true;
}

//-----------------------------------------------------------------------------------------------------------------
void Application::init()
{
    this->display = std::make_shared<X11Display>();
    this->context = std::make_shared<Context>(this->display);
    this->statistics = std::unique_ptr<Statistics>(new Statistics());

    this->initWindow(this->windowMode);

    this->bar = std::unique_ptr<Bar>(new Bar());
}

//-----------------------------------------------------------------------------------------------------------------
void Application::release()
{
    this->bar = nullptr;
    
    this->releaseWindow();

    this->statistics = nullptr;
    this->context = nullptr;
    this->display = nullptr;
}

//-----------------------------------------------------------------------------------------------------------------
void Application::initWindow(X11Window::eMode _windowMode)
{
    ASSERT(this->display);
    ASSERT(this->context);
    
    this->releaseWindow();
    
    auto self = this->shared_from_this();
    this->window = std::make_shared<X11Window>(this->display, this->name, 800, 600, _windowMode);
    this->windowMode = _windowMode;

    this->window->getKeyDownSignal().connect(self, &Application::onKeyDown);
    this->window->getKeyUpSignal().connect(self, &Application::onKeyUp);
    this->window->getMouseDownSignal().connect(self, &Application::onMouseDown);
    this->window->getMouseUpSignal().connect(self, &Application::onMouseUp);

    this->context->setWindow(this->window);

    this->gui = std::unique_ptr<ImmediateGui>(new ImmediateGui());
}

//-----------------------------------------------------------------------------------------------------------------
void Application::releaseWindow()
{
    ASSERT(this->context);
    
    this->gui = nullptr;
    this->context->setWindow(nullptr);
    this->window = nullptr;
}

//-----------------------------------------------------------------------------------------------------------------
void Application::run()
{
    bool keepRunning = true;
    bool toogleWindowMode = false;

    while ( keepRunning ) {
        while ( this->window->handleMessages() ) {
            {
                auto measurementScope = this->statistics->measurement("clear");
                glClearColor(this->clearColor.x, this->clearColor.y, this->clearColor.z, this->clearColor.w);
                glClear(GL_COLOR_BUFFER_BIT);
            }

            unsigned int width = 0;
            unsigned int height = 0;
            int mouseX = -1;
            int mouseY = -1;

            {
                auto measurementScope = this->statistics->measurement("window info");
                this->window->getDimension(&width, &height);
                this->window->getMousePosition(&mouseX, &mouseY);
            }

            if ( this->gui ) {
                auto measurementScope = this->statistics->measurement("ImGui: newFrame");
                this->gui->newFrame(width, height, width, height, mouseX, mouseY);
            }

            this->render();

            if ( this->gui ) {
                auto measurementScope = this->statistics->measurement("ImGui: render");
                this->gui->render();
            }

            if ( 1 == this->glFinishBeforeAfterSwap ) {
                auto measurementScope = this->statistics->measurement("glFinish()");
                glFinish();
            }

            {
                auto measurementScope = this->statistics->measurement("swapBuffers");
                this->context->swapBuffers();
            }

            if ( 2 == this->glFinishBeforeAfterSwap ) {
                auto measurementScope = this->statistics->measurement("glFinish()");
                glFinish();
            }

            if ( this->windowMode != this->window->getMode() ) {
                toogleWindowMode = true;
                this->window->close();
            }

            this->statistics->updateTree();
        }

        keepRunning = toogleWindowMode;

        if ( toogleWindowMode ) {
            this->initWindow(this->windowMode);
            toogleWindowMode = false;
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Application::onKeyDown(unsigned int keycode, unsigned int state, char text)
{
    if ( this->gui && this->gui->handleKeyInput(true, keycode, state, text) ) {
        return;
    }

    if ( 0x09 == keycode || 'q' == text ) {
        // ESC or q
        this->window->close();
        return;
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Application::onKeyUp(unsigned int keycode, unsigned int state, char text)
{
    if ( this->gui && this->gui->handleKeyInput(false, keycode, state, text) ) {
        return;
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Application::onMouseDown(unsigned int button, unsigned int /*state*/)
{
    if ( this->gui ) {
        if ( 4 == button ) {
            if ( this->gui->handleMouseScroll(1) ) {
                return;
            }
        } else if ( 5 == button ) {
            if ( this->gui->handleMouseScroll(-1) ) {
                return;
            }
        } else  {
            if ( this->gui->handleMouseInput(true, button) ) {
                return;
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Application::onMouseUp(unsigned int button, unsigned int /*state*/)
{
    if ( this->gui && this->gui->handleMouseInput(false, button) ) {
        return;
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Application::render()
{
    ASSERT(this->statistics);
    ASSERT(this->gui);

    auto measurementScope = this->statistics->measurement("main rendering");

    if ( this->bar ) {
        this->bar->render(*this->statistics);
    }

    if ( this->statistics ) {
        auto measurementScope = this->statistics->measurement("statistics rendering");
        this->statistics->render();
    }

    ImGui::SetNextWindowPos(ImVec2(10,10), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(400,300), ImGuiCond_FirstUseEver);
    ImGui::Begin("settings");
    if ( ImGui::Button("exit [q/ESC]") ) {
        this->window->close();
    }
    ImGui::Spacing();
    
    ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_DefaultOpen;

    if ( this->bar  ) {
        ImGui::Separator();
        if ( ImGui::TreeNodeEx((void*)this->bar.get(), flags, "bar settings") ) {
            this->bar->options();
            ImGui::TreePop();
        }
    }

    ImGui::Separator();
    if ( ImGui::TreeNodeEx((void*)this, flags, "EGL/X11 settings") ) {

        ImGui::ColorEdit4("clear color", &this->clearColor.x);

        // changes will be handled in the main loop
        int mode = static_cast<int>(this->windowMode);
        if ( ImGui::Combo("window mode", &mode, "windowed\0fullscreen\0override redirect\0\0") ) {
            this->windowMode = static_cast<X11Window::eMode>(mode);
        }

        int swapInterval = this->context->getSwapInterval();
        if ( ImGui::SliderInt("swap interval", &swapInterval, this->context->getMinSwapInterval(), this->context->getMaxSwapInterval()) ) {
            swapInterval = std::max(this->context->getMinSwapInterval(), std::min(swapInterval, this->context->getMaxSwapInterval()));
            this->context->setSwapInterval(swapInterval);
        }

        ImGui::Combo("glFinish()", &this->glFinishBeforeAfterSwap, "disabled\0before swap\0after swap\0\0");

        ImGui::TreePop();
    }
    
    ImGui::End();

 /*   bool show_demo_window = true;
    if (show_demo_window)
    {
        ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver); // Normally user code doesn't need/want to call this because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
        ImGui::ShowDemoWindow(&show_demo_window);
    }*/
}