/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * File:   ImGui.cpp
 * Author: marcel.barkholz
 * 
 * Created on 24. Februar 2018, 15:07
 */

#include "gui/ImmediateGui.h"

#include <iostream>
#include <chrono>

using namespace egltt;

//-----------------------------------------------------------------------------------------------------------------
ImmediateGui::ImmediateGui()
    :   time(0.0f),
        fontTexture(0),
        shaderHandle(0),
        vertHandle(0),
        fragHandle(0),
        attribLocationTex(0),
        attribLocationProjMtx(0),
        attribLocationPosition(0),
        attribLocationUV(0),
        attribLocationColor(0),
        vboHandle(0),
        elementsHandle(0),
        framebufferWidth(0),
        framebufferHeight(0)
{
    this->mouseJustPressed[0] = false;
    this->mouseJustPressed[1] = false;
    this->mouseJustPressed[2] = false;

    this->mousePressed[0] = false;
    this->mousePressed[1] = false;
    this->mousePressed[2] = false;

    this->init();
}

//-----------------------------------------------------------------------------------------------------------------
ImmediateGui::~ImmediateGui()
{
    this->release();
}

//-----------------------------------------------------------------------------------------------------------------
void ImmediateGui::init()
{
    ImGui::CreateContext();
    
    ImGuiIO& io = ImGui::GetIO();
    io.KeyMap[ImGuiKey_Tab] = 0x17;
    io.KeyMap[ImGuiKey_LeftArrow] = 0x71;
    io.KeyMap[ImGuiKey_RightArrow] = 0x72;
    io.KeyMap[ImGuiKey_UpArrow] = 0x6f;
    io.KeyMap[ImGuiKey_DownArrow] = 0x74;
    io.KeyMap[ImGuiKey_PageUp] = 0x70;
    io.KeyMap[ImGuiKey_PageDown] = 0x75;
    io.KeyMap[ImGuiKey_Home] = 0x6e;
    io.KeyMap[ImGuiKey_End] = 0x73;
    io.KeyMap[ImGuiKey_Insert] = 0x76;
    io.KeyMap[ImGuiKey_Delete] = 0x77;
    io.KeyMap[ImGuiKey_Backspace] = 0x16;
    io.KeyMap[ImGuiKey_Space] = 0x41;
    io.KeyMap[ImGuiKey_Enter] = 0x24;
    io.KeyMap[ImGuiKey_Escape] = 0x09;
    io.KeyMap[ImGuiKey_A] = 0x26;
    io.KeyMap[ImGuiKey_C] = 0x36;
    io.KeyMap[ImGuiKey_V] = 0x37;
    io.KeyMap[ImGuiKey_X] = 0x35;
    io.KeyMap[ImGuiKey_Y] = 0x34;
    io.KeyMap[ImGuiKey_Z] = 0x1d;
    
   /* io.SetClipboardTextFn = ;
    io.GetClipboardTextFn = ;
    io.ClipboardUserData = ;*/
}

//-----------------------------------------------------------------------------------------------------------------
void ImmediateGui::release()
{
    this->invalidateDeviceObjects();
    ImGui::DestroyContext();
}

//-----------------------------------------------------------------------------------------------------------------
bool ImmediateGui::createDeviceObjects()
{
    // Backup GL state
    GLint last_texture, last_array_buffer, last_vertex_array;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
    GLCHECKERROR;

    const GLchar *vertex_shader =
        "#version 100\n"
        "uniform mat4 ProjMtx;\n"
        "attribute vec2 Position;\n"
        "attribute vec2 UV;\n"
        "attribute vec4 Color;\n"
        "varying vec2 Frag_UV;\n"
        "varying vec4 Frag_Color;\n"
        "void main()\n"
        "{\n"
        "	Frag_UV = UV;\n"
        "	Frag_Color = Color;\n"
        "	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
        "}\n";

    const GLchar* fragment_shader =
        "#version 100\n"
        "precision mediump float;\n"
        "uniform sampler2D Texture;\n"
        "varying vec2 Frag_UV;\n"
        "varying vec4 Frag_Color;\n"
        "void main()\n"
        "{\n"
        "	gl_FragColor = Frag_Color * texture2D( Texture, Frag_UV.st);\n"
        "}\n";

    this->shaderHandle = glCreateProgram();
    this->vertHandle = glCreateShader(GL_VERTEX_SHADER);
    this->fragHandle = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(this->vertHandle, 1, &vertex_shader, 0);
    glShaderSource(this->fragHandle, 1, &fragment_shader, 0);

    glCompileShader(this->vertHandle);
    GLCHECKSHADERCOMPILEERROR(this->vertHandle);

    glCompileShader(this->fragHandle);
    GLCHECKSHADERCOMPILEERROR(this->fragHandle);

    glAttachShader(this->shaderHandle, this->vertHandle);
    glAttachShader(this->shaderHandle, this->fragHandle);
    glLinkProgram(this->shaderHandle);
    GLCHECKPROGRAMLINKERROR(this->shaderHandle);

    this->attribLocationTex = glGetUniformLocation(this->shaderHandle, "Texture");
    this->attribLocationProjMtx = glGetUniformLocation(this->shaderHandle, "ProjMtx");
    this->attribLocationPosition = glGetAttribLocation(this->shaderHandle, "Position");
    this->attribLocationUV = glGetAttribLocation(this->shaderHandle, "UV");
    this->attribLocationColor = glGetAttribLocation(this->shaderHandle, "Color");

    glGenBuffers(1, &this->vboHandle);
    glGenBuffers(1, &this->elementsHandle);
    GLCHECKERROR;

    this->createFontsTexture();

    // Restore modified GL state
    glBindTexture(GL_TEXTURE_2D, last_texture);
    glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
    glBindVertexArray(last_vertex_array);

    GLCHECKERROR;
}

//-----------------------------------------------------------------------------------------------------------------
void ImmediateGui::invalidateDeviceObjects()
{
    if (this->vboHandle) glDeleteBuffers(1, &this->vboHandle);
    if (this->elementsHandle) glDeleteBuffers(1, &this->elementsHandle);
    this->vboHandle = this->elementsHandle = 0;

    if (this->shaderHandle && this->vertHandle) glDetachShader(this->shaderHandle, this->vertHandle);
    if (this->vertHandle) glDeleteShader(this->vertHandle);
    this->vertHandle = 0;

    if (this->shaderHandle && this->fragHandle) glDetachShader(this->shaderHandle, this->fragHandle);
    if (this->fragHandle) glDeleteShader(this->fragHandle);
    this->fragHandle = 0;

    if (this->shaderHandle) glDeleteProgram(this->shaderHandle);
    this->shaderHandle = 0;

    if (this->fontTexture)
    {
        glDeleteTextures(1, &this->fontTexture);
        ImGui::GetIO().Fonts->TexID = 0;
        this->fontTexture = 0;
    }
}

//-----------------------------------------------------------------------------------------------------------------
void ImmediateGui::createFontsTexture()
{

    GLCHECKERROR;
    // Build texture atlas
    ImGuiIO& io = ImGui::GetIO();
    unsigned char* pixels;
    int width, height;
    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);   // Load as RGBA 32-bits (75% of the memory is wasted, but default font is so small) because it is more likely to be compatible with user's existing shaders. If your ImTextureId represent a higher-level concept than just a GL texture id, consider calling GetTexDataAsAlpha8() instead to save on GPU memory.

    // Upload texture to graphics system
    GLint last_texture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    glGenTextures(1, &this->fontTexture);
    glBindTexture(GL_TEXTURE_2D, this->fontTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    GLCHECKERROR;

    // Store our identifier
    io.Fonts->TexID = (void *)(intptr_t)this->fontTexture;

    // Restore state
    glBindTexture(GL_TEXTURE_2D, last_texture);

    GLCHECKERROR;
}

//-----------------------------------------------------------------------------------------------------------------
void ImmediateGui::newFrame(int windowWidth, int windowHeight, int fbWidth, int fbHeight, int mouseX, int mouseY)
{
    if (!this->fontTexture) {
        this->createDeviceObjects();
    }

    ImGuiIO& io = ImGui::GetIO();

    this->framebufferWidth  = fbWidth;
    this->framebufferHeight = fbHeight;
    io.DisplaySize = ImVec2((float)windowWidth, (float)windowHeight);
    io.DisplayFramebufferScale = ImVec2(windowWidth > 0 ? ((float)fbWidth / windowWidth) : 0, windowHeight > 0 ? ((float)fbHeight / windowHeight) : 0);

    // Setup time step
    std::chrono::duration<double> sinceEpoch = std::chrono::system_clock::now().time_since_epoch();
    double current_time =  sinceEpoch.count();
    io.DeltaTime = this->time > 0.0 ? (float)(current_time - this->time) : (float)(1.0f/60.0f);
    this->time = current_time;

    // Setup inputs
    // (we already got mouse wheel, keyboard keys & characters from glfw callbacks polled in glfwPollEvents())
    if ( -1 != mouseX && -1 != mouseY)
    {
        if (io.WantMoveMouse)
        {
            std::cerr << "move mouse unimplemented" << std::endl;
           // glfwSetCursorPos(g_Window, (double)io.MousePos.x, (double)io.MousePos.y);   // Set mouse position if requested by io.WantMoveMouse flag (used when io.NavMovesTrue is enabled by user and using directional navigation)
        }
        else
        {
            io.MousePos = ImVec2((float)mouseX, (float)mouseY);
        }
    }
    else
    {
        io.MousePos = ImVec2(-FLT_MAX,-FLT_MAX);
    }

    for (int i = 0; i < 3; i++)
    {
        // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
        io.MouseDown[i] = this->mouseJustPressed[i] || this->mousePressed[i];
        this->mouseJustPressed[i] = false;
    }

    // Start the frame. This call will update the io.WantCaptureMouse, io.WantCaptureKeyboard flag that you can use to dispatch inputs (or not) to your application.
    ImGui::NewFrame();
}

//-----------------------------------------------------------------------------------------------------------------
void ImmediateGui::render()
{
    glViewport(0, 0, this->framebufferWidth, this->framebufferHeight);
    ImGui::Render();
    this->renderDrawData(ImGui::GetDrawData());
    GLCHECKERROR;
}

bool ImmediateGui::handleKeyInput(bool keyPress, unsigned int keycode, unsigned int state, char text)
{
    ImGuiIO& io = ImGui::GetIO();
    io.KeysDown[keycode] = keyPress;

    io.KeyCtrl  = state & ControlMask;
    io.KeyShift = state & ShiftMask;
    io.KeyAlt   = state & Mod5Mask;
   // io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];

    if ( keyPress && text > 0 && text < 0x10000 ) {
        io.AddInputCharacter((unsigned short)text);
    }

    return io.WantCaptureKeyboard;
}

//-----------------------------------------------------------------------------------------------------------------
bool ImmediateGui::handleMouseInput(bool mousePress, unsigned int button)
{
    ImGuiIO& io = ImGui::GetIO();

    if ( button > 0 && button <= 3 ) {
        this->mousePressed[button-1] = mousePress;
        if ( mousePress ) {
            this->mouseJustPressed[button-1] = true;
        }
    }

    return io.WantCaptureMouse;
}

//-----------------------------------------------------------------------------------------------------------------
bool ImmediateGui::handleMouseScroll(int scroll)
{
    ImGuiIO& io = ImGui::GetIO();
    io.MouseWheel += (float)scroll;
    return io.WantCaptureMouse;
}

//-----------------------------------------------------------------------------------------------------------------
void ImmediateGui::renderDrawData(ImDrawData* draw_data)
{
    // Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
    ImGuiIO& io = ImGui::GetIO();
    int fb_width = (int)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
    int fb_height = (int)(io.DisplaySize.y * io.DisplayFramebufferScale.y);
    if (fb_width == 0 || fb_height == 0)
        return;
    draw_data->ScaleClipRects(io.DisplayFramebufferScale);

    // Backup GL state
    GLenum last_active_texture; glGetIntegerv(GL_ACTIVE_TEXTURE, (GLint*)&last_active_texture);
    glActiveTexture(GL_TEXTURE0);
    GLint last_program; glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
    GLint last_texture; glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    //GLint last_sampler; glGetIntegerv(GL_SAMPLER_BINDING, &last_sampler);
    GLint last_array_buffer; glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
    GLint last_element_array_buffer; glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
    GLint last_vertex_array; glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
    //GLint last_polygon_mode[2]; glGetIntegerv(GL_POLYGON_MODE, last_polygon_mode);
    GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
    GLint last_scissor_box[4]; glGetIntegerv(GL_SCISSOR_BOX, last_scissor_box);
    GLenum last_blend_src_rgb; glGetIntegerv(GL_BLEND_SRC_RGB, (GLint*)&last_blend_src_rgb);
    GLenum last_blend_dst_rgb; glGetIntegerv(GL_BLEND_DST_RGB, (GLint*)&last_blend_dst_rgb);
    GLenum last_blend_src_alpha; glGetIntegerv(GL_BLEND_SRC_ALPHA, (GLint*)&last_blend_src_alpha);
    GLenum last_blend_dst_alpha; glGetIntegerv(GL_BLEND_DST_ALPHA, (GLint*)&last_blend_dst_alpha);
    GLenum last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, (GLint*)&last_blend_equation_rgb);
    GLenum last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, (GLint*)&last_blend_equation_alpha);
    GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
    GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
    GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
    GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);

    // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled, polygon fill
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    // Setup viewport, orthographic projection matrix
    glViewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);
    const float ortho_projection[4][4] =
    {
        { 2.0f/io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
        { 0.0f,                  2.0f/-io.DisplaySize.y, 0.0f, 0.0f },
        { 0.0f,                  0.0f,                  -1.0f, 0.0f },
        {-1.0f,                  1.0f,                   0.0f, 1.0f },
    };
    glUseProgram(this->shaderHandle);
    glUniform1i(this->attribLocationTex, 0);
    glUniformMatrix4fv(this->attribLocationProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);
    //glBindSampler(0, 0); // Rely on combined texture/sampler state.

    // Recreate the VAO every time
    // (This is to easily allow multiple GL contexts. VAO are not shared among GL contexts, and we don't track creation/deletion of windows so we don't have an obvious key to use to cache them.)
    GLuint vao_handle = 0;
    glGenVertexArrays(1, &vao_handle);
    glBindVertexArray(vao_handle);
    glBindBuffer(GL_ARRAY_BUFFER, this->vboHandle);
    glEnableVertexAttribArray(this->attribLocationPosition);
    glEnableVertexAttribArray(this->attribLocationUV);
    glEnableVertexAttribArray(this->attribLocationColor);
    glVertexAttribPointer(this->attribLocationPosition, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, pos));
    glVertexAttribPointer(this->attribLocationUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, uv));
    glVertexAttribPointer(this->attribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, col));

    // Draw
    for (int n = 0; n < draw_data->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list = draw_data->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = 0;

        glBindBuffer(GL_ARRAY_BUFFER, this->vboHandle);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert), (const GLvoid*)cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->elementsHandle);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx), (const GLvoid*)cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);

        for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
            const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
            if (pcmd->UserCallback)
            {
                pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
                glScissor((int)pcmd->ClipRect.x, (int)(fb_height - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
                glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
            }
            idx_buffer_offset += pcmd->ElemCount;
        }
    }
    glDeleteVertexArrays(1, &vao_handle);

    // Restore modified GL state
    glUseProgram(last_program);
    glBindTexture(GL_TEXTURE_2D, last_texture);
    //glBindSampler(0, last_sampler);
    glActiveTexture(last_active_texture);
    glBindVertexArray(last_vertex_array);
    glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
    glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
    glBlendFuncSeparate(last_blend_src_rgb, last_blend_dst_rgb, last_blend_src_alpha, last_blend_dst_alpha);
    if (last_enable_blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
    if (last_enable_cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
    if (last_enable_depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
    if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST); else glDisable(GL_SCISSOR_TEST);
    //glPolygonMode(GL_FRONT_AND_BACK, (GLenum)last_polygon_mode[0]);
    glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
    glScissor(last_scissor_box[0], last_scissor_box[1], (GLsizei)last_scissor_box[2], (GLsizei)last_scissor_box[3]);
}

