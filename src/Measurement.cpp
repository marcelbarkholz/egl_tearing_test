/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * File:   Measurement.cpp
 * Author: marcel.barkholz
 * 
 * Created on 25. Februar 2018, 17:04
 */

#include "Measurement.h"
#include "common/Assertion.h"

using namespace egltt;

//------------------------------------------------------------------------------------------------------------------------------------------------------------
MeasurementScope::MeasurementScope(const std::shared_ptr<Measurement>& _measurement)
    :   measurement(_measurement)
{
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------
MeasurementScope::~MeasurementScope()
{
}

//-----------------------------------------------------------------------------------------------------------------
Measurement::Measurement(const std::shared_ptr<Measurement>& _parent, const std::string& _name)
    :   parent(_parent),
        name(_name),
        framecount(0),
        running(false),
        ended(false),
        index(0),
        start(0.0f),
        end(0.0f),
        lastRequestedChild(0)
{
}

//-----------------------------------------------------------------------------------------------------------------
Measurement::~Measurement()
{
}

//-----------------------------------------------------------------------------------------------------------------
std::shared_ptr<Measurement> Measurement::getParent() const
{
    return this->parent.lock();
}

//-----------------------------------------------------------------------------------------------------------------
const std::string& Measurement::getName() const
{
    return this->name;
}

//-----------------------------------------------------------------------------------------------------------------
const size_t& Measurement::getFramecount() const
{
    return this->framecount;
}

//-----------------------------------------------------------------------------------------------------------------
const size_t& Measurement::getIndex() const
{
    return this->index;
}

//-----------------------------------------------------------------------------------------------------------------
bool Measurement::isEnded() const
{
    return this->ended;
}

//-----------------------------------------------------------------------------------------------------------------
const float& Measurement::getStart(bool average) const
{
    ASSERT(this->ended);
    if ( average ) {
        return this->startAverage.getAverage();
    }
    return this->start;
}

//-----------------------------------------------------------------------------------------------------------------
const float& Measurement::getEnd(bool average) const
{
    ASSERT(this->ended);
    if ( average ) {
        return this->endAverage.getAverage();
    }
    return this->end;
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------
std::shared_ptr<MeasurementScope> Measurement::begin(const Key&)
{
    this->running = true;
    this->ended  = false;

    this->startTimestamp = Clock_t::now();

    auto self = this->shared_from_this();
    return std::shared_ptr<MeasurementScope>(new MeasurementScope(self), [this](MeasurementScope* scope) {
        delete scope;
        this->running = false;
        this->ended = true;

        this->endTimestamp = Clock_t::now();
    });
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------
std::shared_ptr<Measurement> Measurement::getChild(const std::string& _name, const Key&)
{
    if ( !this->childs.empty() ) {
        for ( this->lastRequestedChild; this->lastRequestedChild<this->childs.size(); ++this->lastRequestedChild ) {
            if ( this->childs[this->lastRequestedChild]->getName() == _name ) {
                this->childs[this->lastRequestedChild]->touch(this->framecount);
                return this->childs[this->lastRequestedChild];
            }
        }
    }

    auto self = this->shared_from_this();
    std::shared_ptr<Measurement> newChild = std::make_shared<Measurement>(self, _name);
    newChild->touch(this->framecount);

    this->childs.push_back(newChild);

    return newChild;
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------
void Measurement::updateTree(size_t averageSize, const Key&)
{
    this->updateTreeImpl(averageSize, this->startTimestamp, 0);
    this->framecount++;
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------
void Measurement::withTree(size_t depth, std::function<void(const Measurement&, size_t)>& func) const
{
    func(*this, depth);

    for ( const std::shared_ptr<Measurement>& child : this->childs ) {
        child->withTree(depth+1, func);
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------
void Measurement::updateTreeImpl(size_t averageSize, const TimePoint_t& parentStart, size_t _index)
{
    ASSERT(10 <= averageSize);
    ASSERT(this->ended);
    this->start = std::chrono::duration<double, std::milli>(this->startTimestamp - parentStart).count();
    this->end   = std::chrono::duration<double, std::milli>(this->endTimestamp   - parentStart).count();
    ASSERT(0.0f != this->end);

    this->startAverage.setSize(averageSize);
    this->startAverage.push(this->start);

    this->endAverage.setSize(averageSize);
    this->endAverage.push(this->end);

    this->index = _index;

    std::vector<Childs_t::iterator> eraseList;

    Childs_t::iterator iter  = this->childs.begin();
    Childs_t::iterator itEnd = this->childs.end();

    size_t i=0;
    for ( ; iter != itEnd; ++iter ) {
        std::shared_ptr<Measurement>& child = *iter;
        if ( child->getFramecount() != this->framecount ) {
            eraseList.push_back(iter);

        } else {
            child->updateTreeImpl(averageSize, this->startTimestamp, i);
            ++i;
        }       
    }

    for ( Childs_t::iterator& iter : eraseList ) {
        this->childs.erase(iter);
    }

    this->lastRequestedChild = 0;
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------
void Measurement::touch(size_t _framecount)
{
    this->framecount = _framecount;
}
