/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * File:   EglContext.cpp
 * Author: marcel.barkholz
 * 
 * Created on 24. Februar 2018, 10:58
 */

#include "egl/Context.h"
#include "x11/X11Display.h"
#include "x11/X11Window.h"
#include "common/Assertion.h"

#include <iostream>
#include <cstring>
#include <sstream>

using namespace egltt;

#define ES_WINDOW_RGB           0
#define ES_WINDOW_ALPHA         1
#define ES_WINDOW_DEPTH         2
#define ES_WINDOW_STENCIL       4
#define ES_WINDOW_MULTISAMPLE   8


//-----------------------------------------------------------------------------------------------------------------
static EGLint getContextRenderableType(EGLDisplay display )
{
#ifdef EGL_KHR_create_context
   const char *extensions = eglQueryString(display, EGL_EXTENSIONS);

   if ( extensions != NULL && std::strstr( extensions, "EGL_KHR_create_context" ) ) {
      return EGL_OPENGL_ES3_BIT_KHR;
   }
#endif
   return EGL_OPENGL_ES2_BIT;
}

//-----------------------------------------------------------------------------------------------------------------
Context::Context(const std::shared_ptr<X11Display>& _x11Display)
    :   x11Display(_x11Display),
        display(nullptr),
        context(nullptr),
        surface(nullptr),
        majorVersion(0),
        minorVersion(0),
        minSwapInterval(-1),
        maxSwapInterval(-1),
        currentSwapInterval(1)
{
    this->initDisplay();
}

//-----------------------------------------------------------------------------------------------------------------
Context::~Context()
{
    this->releaseDisplay();
}

//-----------------------------------------------------------------------------------------------------------------
void Context::setWindow(const std::shared_ptr<X11Window>& _window)
{
    this->releaseWindow();
    this->window = _window;

    if ( this->window ) {
        this->initWindow();
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Context::initDisplay()
{
    ASSERT(this->x11Display);
    ASSERT(!this->display);

    Display* nativeDisplay = this->x11Display->getNativeDisplay();
    ASSERT(nativeDisplay);

    this->display = eglGetDisplay(nativeDisplay);
    if ( EGL_NO_DISPLAY == this->display ) {
        throw std::runtime_error("eglGetDisplay(..) failed");
    }

    if ( !eglInitialize(this->display, &this->majorVersion, &this->minorVersion) )  {
        throw std::runtime_error("eglInitialize(..) failed");
    }
}


//-----------------------------------------------------------------------------------------------------------------
void Context::releaseDisplay()
{
    this->releaseWindow();

    if ( this->display ) {
        eglTerminate(this->display);
        this->display = nullptr;
    }

    if ( this->x11Display ) {
        Display* nativeDisplay = this->x11Display->getNativeDisplay();
        ASSERT(nativeDisplay);

        XSync(nativeDisplay, false);
        
        this->x11Display = nullptr;
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Context::initWindow()
{
    ASSERT(this->display);
    ASSERT(this->window);
    ASSERT(!this->context);
    ASSERT(!this->surface);

    Window nativeWindow = this->window->getNativeWindow();
    ASSERT(nativeWindow);


    EGLConfig config;
    EGLint contextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };

    {
        GLuint flags = ES_WINDOW_ALPHA | ES_WINDOW_DEPTH | ES_WINDOW_STENCIL;
        EGLint numConfigs = 0;
        EGLint attribList[] = {
            EGL_RED_SIZE,       8,
            EGL_GREEN_SIZE,     8,
            EGL_BLUE_SIZE,      8,
            EGL_ALPHA_SIZE,     ( flags & ES_WINDOW_ALPHA ) ? 8 : EGL_DONT_CARE,
            EGL_DEPTH_SIZE,     ( flags & ES_WINDOW_DEPTH ) ? 24 : EGL_DONT_CARE,
            EGL_STENCIL_SIZE,   ( flags & ES_WINDOW_STENCIL ) ? 8 : EGL_DONT_CARE,
            EGL_SAMPLE_BUFFERS, ( flags & ES_WINDOW_MULTISAMPLE ) ? 1 : 0,
            // if EGL_KHR_create_context extension is supported, then we will use
            // EGL_OPENGL_ES3_BIT_KHR instead of EGL_OPENGL_ES2_BIT in the attribute list
            EGL_RENDERABLE_TYPE, getContextRenderableType(this->display),
            EGL_NONE
        };

        if ( !eglChooseConfig(this->display, attribList, &config, 1, &numConfigs)) {
            throw std::runtime_error("eglChooseConfig(..) failed");
        }

        if ( numConfigs < 1 ) {
            throw std::runtime_error("no egl configs found");
        }
    }

    this->surface = eglCreateWindowSurface(this->display, config, nativeWindow, NULL);
    if ( EGL_NO_SURFACE == this->surface ) {
        throw std::runtime_error("eglCreateWindowSurface(..) failed");
    }

    this->context = eglCreateContext(this->display, config, EGL_NO_CONTEXT, contextAttribs);
    if ( EGL_NO_CONTEXT == this->context ) {
        throw std::runtime_error("eglCreateContext(..) failed");
    }

    if ( !eglGetConfigAttrib(this->display, config, EGL_MIN_SWAP_INTERVAL, &this->minSwapInterval) ) {
        throw std::runtime_error("eglGetConfigAttrib(...EGL_MIN_SWAP_INTERVAL..) failed");
    }
    if ( !eglGetConfigAttrib(this->display, config, EGL_MAX_SWAP_INTERVAL, &this->maxSwapInterval) ) {
        throw std::runtime_error("eglGetConfigAttrib(...EGL_MAX_SWAP_INTERVAL..) failed");
    }

    eglSwapInterval(this->display, this->currentSwapInterval);

    this->makeCurrent();
}


//-----------------------------------------------------------------------------------------------------------------
void Context::releaseWindow()
{
    if ( this->context ) {
        ASSERT(this->display);
        eglMakeCurrent(this->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroyContext(this->display, this->context);
        this->context = nullptr;
    }

    if ( this->surface ) {
        ASSERT(this->display);
        eglDestroySurface(this->display, this->surface);
        this->surface = nullptr;
    }

    this->window = nullptr;
}

//-----------------------------------------------------------------------------------------------------------------
void Context::makeCurrent()
{
    ASSERT(this->display);
    ASSERT(this->context);
    ASSERT(this->surface);

    if ( !eglMakeCurrent(this->display, this->surface, this->surface, this->context) ) {
        throw std::runtime_error("eglMakeCurrent(..) failed");
    }
}


//-----------------------------------------------------------------------------------------------------------------
void Context::swapBuffers()
{
    ASSERT(this->display);
    ASSERT(this->context);
    ASSERT(this->surface);

    if ( !eglSwapBuffers(this->display, this->surface) ) {
        std::cerr << "eglSwapBuffers(...) failed" << std::endl;
    }
}

//-----------------------------------------------------------------------------------------------------------------
void Context::setSwapInterval(int interval)
{
    ASSERT(this->display);
    ASSERT(this->context);
    ASSERT(this->surface);
    ASSERT(this->minSwapInterval <= interval);
    ASSERT(this->maxSwapInterval >= interval);

    if ( this->currentSwapInterval == interval ) {
        return;
    }

    if ( !eglSwapInterval(this->display, interval) ) {
        std::cerr << "eglSwapInterval(...) failed" << std::endl;
    }

    this->currentSwapInterval = interval;
}

//-----------------------------------------------------------------------------------------------------------------
int Context::getSwapInterval() const
{
    return this->currentSwapInterval;
}

//-----------------------------------------------------------------------------------------------------------------
int Context::getMinSwapInterval() const
{
    return this->minSwapInterval;
}

//-----------------------------------------------------------------------------------------------------------------
int Context::getMaxSwapInterval() const
{
    return this->maxSwapInterval;
}


//-----------------------------------------------------------------------------------------------------------------
std::string Context::eglErrorString(GLenum error)
{
    switch(error)  {
    case EGL_SUCCESS: return "No error";
    case EGL_NOT_INITIALIZED: return "EGL not initialized or failed to initialize";
    case EGL_BAD_ACCESS: return "Resource inaccessible";
    case EGL_BAD_ALLOC: return "Cannot allocate resources";
    case EGL_BAD_ATTRIBUTE: return "Unrecognized attribute or attribute value";
    case EGL_BAD_CONTEXT: return "Invalid EGL context";
    case EGL_BAD_CONFIG: return "Invalid EGL frame buffer configuration";
    case EGL_BAD_CURRENT_SURFACE: return "Current surface is no longer valid";
    case EGL_BAD_DISPLAY: return "Invalid EGL display";
    case EGL_BAD_SURFACE: return "Invalid surface";
    case EGL_BAD_MATCH: return "Inconsistent arguments";
    case EGL_BAD_PARAMETER: return "Invalid argument";
    case EGL_BAD_NATIVE_PIXMAP: return "Invalid native pixmap";
    case EGL_BAD_NATIVE_WINDOW: return "Invalid native window";
    case EGL_CONTEXT_LOST: return "Context lost";
    }
    std::stringstream sstr;
    sstr << "Unknown error 0x" << std::hex << int(error);
    return sstr.str();
}

//-----------------------------------------------------------------------------------------------------------------
std::string Context::glErrorString(GLenum error)
{
    switch(error)  {
    case GL_NO_ERROR: return "No error";
    case GL_INVALID_ENUM: return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE: return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION: return "GL_INVALID_OPERATION";
    case GL_INVALID_FRAMEBUFFER_OPERATION: return "GL_INVALID_FRAMEBUFFER_OPERATION";
    case GL_OUT_OF_MEMORY: return "GL_OUT_OF_MEMORY";
    }
    std::stringstream sstr;
    sstr << "Unknown error 0x" << std::hex << int(error);
    return sstr.str();
}

//-----------------------------------------------------------------------------------------------------------------
void Context::checkErrors(std::string file, std::string function, unsigned int line)
{
    GLenum err = glGetError();
    if ( GL_NO_ERROR != err ) {
        std::stringstream sstr;
        sstr << "GL error: "
             << glErrorString(err)
             << ", in " << function
             << " at " << file << ":" << line;

        throw std::runtime_error(sstr.str());
    }
}

//-----------------------------------------------------------------------------------------------------------------
bool Context::checkShaderCompileErrors(GLuint shader, std::string& error)
{
    GLint compiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

    if ( compiled ) {
        return true;
    }

    const int infoLogLength = 1024;
    GLchar* infoLog = new GLchar[infoLogLength + 1];
    glGetShaderInfoLog(shader, infoLogLength, (GLsizei*)NULL, infoLog);

    error = infoLog;

    delete [] infoLog;

    return false;
}

//-----------------------------------------------------------------------------------------------------------------
void Context::checkShaderCompileErrors(GLuint shader, std::string file, std::string function, unsigned int line)
{
    std::string error;
    if ( !checkShaderCompileErrors(shader, error) ) {

        std::stringstream sstr;
        sstr << "Shader compile  error: \n"
             << error
             << "\n" << function
             << " at " << file << ":" << line;

        throw std::runtime_error(sstr.str());
    }
}

//-----------------------------------------------------------------------------------------------------------------
bool Context::checkProgramLinkErrors(GLuint program, std::string& error)
{
    GLint linked;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);

    if ( linked ) {
        return true;
    }

    int infoLogLength = 1024;
    GLchar* infoLog = new GLchar[infoLogLength + 1];
    glGetProgramInfoLog(program, infoLogLength, (GLsizei*)NULL, infoLog);

    if ( strlen(infoLog) > 0 ) {
        error = infoLog;
    }

    delete [] infoLog;

    return false;
}

//-----------------------------------------------------------------------------------------------------------------
void Context::checkProgramLinkErrors(GLuint program, std::string file, std::string function, unsigned int line)
{
    std::string error;
    if ( !checkProgramLinkErrors(program, error) ) {

        std::stringstream sstr;
        sstr << "Program link  error: \n"
             << error
             << "\n" << function
             << " at " << file << ":" << line;

        throw std::runtime_error(sstr.str());
    }
}
