/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   EglContext.h
 * Author: marcel.barkholz
 *
 * Created on 24. Februar 2018, 10:58
 */

#ifndef CONTEXT_H
#define CONTEXT_H

#include <GLES3/gl3.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <string>
#include <memory>

#define GLCHECKERROR egltt::Context::checkErrors(__FILE__,  __PRETTY_FUNCTION__, __LINE__);
#define GLCHECKSHADERCOMPILEERROR(shader) egltt::Context::checkShaderCompileErrors(shader, __FILE__,  __PRETTY_FUNCTION__, __LINE__);
#define GLCHECKPROGRAMLINKERROR(program) egltt::Context::checkProgramLinkErrors(program, __FILE__,  __PRETTY_FUNCTION__, __LINE__);

namespace egltt {

    class X11Display;
    class X11Window;

    //-----------------------------------------------------------------------------------------------------------------
    //! \class Context
    class Context
    {
    public:
        Context(const std::shared_ptr<X11Display>& _x11Display);
        virtual ~Context();

        void setWindow(const std::shared_ptr<X11Window>& _window);

        void makeCurrent();
        void swapBuffers();
        void setSwapInterval(int interval);
        int getSwapInterval() const;
        int getMinSwapInterval() const;
        int getMaxSwapInterval() const;

        static std::string eglErrorString(GLenum error);
        static std::string glErrorString(GLenum error);
        static void checkErrors(std::string file, std::string function, unsigned int line);

        static bool checkShaderCompileErrors(GLuint shader, std::string& error);
        static void checkShaderCompileErrors(GLuint shader, std::string file, std::string function, unsigned int line);

        static bool checkProgramLinkErrors(GLuint program, std::string& error);
        static void checkProgramLinkErrors(GLuint program, std::string file, std::string function, unsigned int line);

    private:
        void initDisplay();
        void releaseDisplay();

        void initWindow();
        void releaseWindow();

    private:
        std::shared_ptr<X11Display> x11Display;
        EGLDisplay display;

        std::shared_ptr<X11Window> window;
        EGLContext context;
        EGLSurface surface;

        EGLint majorVersion;
        EGLint minorVersion;

        EGLint minSwapInterval;
        EGLint maxSwapInterval;
        EGLint currentSwapInterval;
    };

} // namespace egltt

#endif /* CONTEXT_H */

