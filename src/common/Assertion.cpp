/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "common/Assertion.h"
#include <sstream>

using namespace egltt;

//-----------------------------------------------------------------------------------------------------------------
Assertion::Assertion(const std::string& _message, const std::string& _file, unsigned int _line, const std::string& _function)
    :   message(nullptr)
{
    std::stringstream sstr;
    sstr << "Assertion failure: " << _message << std::endl
         << "Function:          " << _function << std::endl
         << "File:              " <<  _file << std::endl
         << "Line:              " <<  _line << std::endl;

    size_t length = sstr.str().length()+1;

    this->message = new char [length];
    std::snprintf(this->message, length, "%s", sstr.str().c_str());
}

//-----------------------------------------------------------------------------------------------------------------
Assertion::~Assertion()
{
    if ( this->message ) {
        delete [] this->message;
        this->message = nullptr;
    }
}

//-----------------------------------------------------------------------------------------------------------------
const char* Assertion::what() const noexcept
{
    if ( this->message ) {
        return this->message;
    }

    return "Assertion failure but no message...";
}