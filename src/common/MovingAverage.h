/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   MovingAverage.h
 * Author: marcel.barkholz
 *
 * Created on 25. Februar 2018, 20:20
 */

#ifndef MOVINGAVERAGE_H
#define MOVINGAVERAGE_H

#include <queue>

namespace egltt {

    class MovingAverage {
    public:
        MovingAverage();
        virtual ~MovingAverage();

        void setSize(size_t _size);
        size_t getSize() const;

        const float& getAverage() const;

        void push(float value);

    private:
        size_t size;

        std::queue<double> values;
        double average;
        float floatAverage;



    };

} // namespace egltt

#endif /* MOVINGAVERAGE_H */

