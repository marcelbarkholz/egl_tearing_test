/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   Measurement.h
 * Author: marcel.barkholz
 *
 * Created on 25. Februar 2018, 17:04
 */

#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#include <memory>
#include <chrono>
#include <vector>
#include <functional>

#include "common/MovingAverage.h"

namespace egltt {

    class Measurement;

    //-----------------------------------------------------------------------------------------------------------------
    //! \class MeasurementScope
    class MeasurementScope
    {
    public:
        MeasurementScope(const std::shared_ptr<Measurement>& _measurement);
        virtual ~MeasurementScope();

    private:
         std::shared_ptr<Measurement> measurement;
    };

    //-----------------------------------------------------------------------------------------------------------------
    //! \class Measurement
    class Measurement : public std::enable_shared_from_this<Measurement>
    {
    public:
        typedef std::chrono::high_resolution_clock Clock_t;
        typedef Clock_t::time_point TimePoint_t;

        class Key {
        private:
            friend class Statistics;
            Key() {}
            Key(Key const&) {}
        };

        Measurement(const std::shared_ptr<Measurement>& _parent, const std::string& _name);
        virtual ~Measurement();

        std::shared_ptr<Measurement> getParent() const;

        const std::string& getName() const;
        const size_t& getFramecount() const;
        const size_t& getIndex() const;
        bool isEnded() const;

        const float& getStart(bool average=false) const;
        const float& getEnd(bool average=false) const;

        std::shared_ptr<MeasurementScope> begin(const Key&);

        std::shared_ptr<Measurement> getChild(const std::string& _name, const Key&);

        void withTree(size_t depth, std::function<void(const Measurement&, size_t)>& func) const;

        void updateTree(size_t averageSize, const Key&);

    private:
        void updateTreeImpl(size_t averageSize, const TimePoint_t& parentStart, size_t _index);
        void touch(size_t _framecount);


    private:
        std::weak_ptr<Measurement> parent;
        std::string name;
        size_t framecount;
        bool running;
        bool ended;
        size_t index;

        TimePoint_t startTimestamp;
        TimePoint_t endTimestamp;

        float start;
        float end;

        MovingAverage startAverage;
        MovingAverage endAverage;

        typedef std::vector<std::shared_ptr<Measurement>> Childs_t;
        Childs_t childs;
        size_t lastRequestedChild;

    };

} // namespace egltt

#endif /* MEASUREMENT_H */

